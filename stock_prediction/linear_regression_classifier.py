import datetime, os
import pandas as pd
import numpy as np
from sklearn import preprocessing, cross_validation
from sklearn.linear_model import LinearRegression
from utils import unpickle, save_pickle

def creates_trains_and_saves_classifier(df, quandl_code, forecast_out):

    X = np.array(df.drop(['label'], 1))
    X = preprocessing.scale(X)
    # Get all except the 'forecast_out' last
    X = X[:-forecast_out]
    # Get the 'forecast_out' last
    X_lately = X[-forecast_out:]

    df.dropna(inplace=True)
    y = np.array(df['label'])

    X_train, X_test, y_train, y_test = cross_validation.train_test_split(X, y, test_size=0.2)

    classifier = LinearRegression(n_jobs=-1)
    classifier.fit(X_train, y_train)
    accuracy = classifier.score(X_test, y_test) * 100.0

    # Gets the prediction of the last 'forecast_out' day(s) as a list of numbers (no dates)
    forecast_prediction = classifier.predict(X_lately)

    print('\nAccuracy: {0}%\n\nForecast time: {1} day(s)\n\nPrediction: {2}\n'.format(accuracy, forecast_out, forecast_prediction))

    # Sets the forecast colomn to NaN for all items
    df['Forecast'] = np.nan

    last_date = df.iloc[-1].name
    last_unix = last_date.timestamp()
    one_day = 86400
    next_unix = last_unix + one_day

    for i in forecast_prediction:
        next_date = datetime.datetime.fromtimestamp(next_unix)
        next_unix += one_day
        df.loc[next_date] = [np.nan for _ in range(len(df.columns)-1)] + [i]

    df['Quandl Code'] = quandl_code
    prediction_df = df[-forecast_out:]
    prediction_df = df[['Forecast', 'Quandl Code']]
    df = df[:-forecast_out]

    quandl_code_dir = 'results/' + quandl_code.replace("\\", "\\\\")
    if not os.path.exists(quandl_code_dir):
        os.makedirs(quandl_code_dir)

    save_pickle(prediction_df, (quandl_code_dir + '/prediction_df.pkl'))
    save_pickle(df, (quandl_code_dir + '/training_df.pkl'))
    save_pickle(classifier, (quandl_code_dir + '/classifier.pkl'))
