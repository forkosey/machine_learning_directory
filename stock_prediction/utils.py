import pickle

def unpickle(file_name):
    with open(file_name, 'rb') as opened_file:
        data = pickle.load(opened_file, encoding='bytes')
    return data


def save_pickle(data, file_name):
    pickle.dump(data, open(file_name, 'wb'))
