import quandl, math, datetime
import pandas as pd
import numpy as np
from sklearn import preprocessing, cross_validation, svm
from sklearn.linear_model import LinearRegression
from credentials import QUANDL_API_KEY
from linear_regression_classifier import creates_trains_and_saves_classifier

quandl.ApiConfig.api_key = QUANDL_API_KEY

data_set = quandl.get('WIKI/GOOGL')

data_set = data_set[['Adj. Open', 'Adj. High', 'Adj. Low', 'Adj. Close', 'Adj. Volume']]
data_set['HL_percent'] = (data_set['Adj. High'] - data_set['Adj. Close']) / data_set['Adj. Close'] * 100.0
data_set['percent_change'] = (data_set['Adj. Close'] - data_set['Adj. Open']) / data_set['Adj. Open'] * 100.0
data_set = data_set[['Adj. Close', 'HL_percent', 'percent_change', 'Adj. Volume']]

forecast_col = 'Adj. Close'
data_set.fillna(-99999, inplace=True)

forecast_out = int(math.ceil(0.001*len(data_set)))

# Label column is the search data (the cost in 'forecast_out' day(s))
data_set['label'] = data_set[forecast_col].shift(-forecast_out)

creates_trains_and_saves_classifier(data_set, 'WIKI\GOOGL', forecast_out)
