from pprint import pprint
import _pickle as pickle
import numpy as np
import tensorflow as tf

def unpickle(file_name):
    with open(file_name, 'rb') as opened_file:
        data = pickle.load(opened_file, encoding='bytes')
    return data


def print_data_batch_info():
    batch = unpickle('./cifar-10-batches-py/data_batch_1')
    print('\n=========== Batch keys name ============\n')
    for item in batch:
        pprint(item)

    print('\n=========== File names ============\n')
    for item in batch[b'filenames'][:5]:
        pprint(item)

    print('\n=========== Data ============\n')
    batch[b'data'] = batch[b'data'].reshape([-1, 3, 32, 32])
    for item in batch[b'data'][:5]:
        pprint(item)

    print('\n=========== Labels ============\n')
    for item in batch[b'labels'][:5]:
        pprint(item)


def save_pickle(data, file_name):
    pickle.dump(data, open(file_name, 'wb'))


def print_pkl_training_files():
    create_training_pkl_file()
    training_dataset = unpickle('training_dataset.pkl')
    pprint(training_dataset)


def print_meta_pkl_file():
    pkl_file = unpickle('./cifar-10-batches-py/batches.meta')
    i = 0
    print('\n')
    for label in pkl_file[b'label_names']:
        print(str(i) + ' : ' + str(label))
        i+=1


def create_training_pkl_file():
    img_arrays_list = []
    is_bird_boolean_list = []
    training_dataset = []

    for i in range(1,6):
        batch = unpickle('./cifar-10-batches-py/data_batch_' + str(i))

        img_arrays_list.extend(batch[b"data"].reshape([-1, 3, 32, 32]))

        for label in batch[b'labels']:
            is_bird_boolean_list.append(label==2)

    training_dataset.append(img_arrays_list)
    training_dataset.append(is_bird_boolean_list)

    save_pickle(training_dataset, './training_dataset.pkl')

    print('\nTraining dataset created.\n')


def create_testing_pkl_file():
    batch = unpickle('./cifar-10-batches-py/test_batch')

    img_arrays_list = []
    is_bird_boolean_list = []
    testing_dataset = []

    img_arrays_list.extend(batch[b'data'].reshape([-1, 3, 32, 32]))

    for label in batch[b'labels']:
        is_bird_boolean_list.append(label==2)

    testing_dataset.append(img_arrays_list)
    testing_dataset.append(is_bird_boolean_list)

    save_pickle(testing_dataset, './testing_dataset.pkl')

    print('Testing dataset created.\n')


if __name__ == "__main__":
    #print_data_batch_info()
    #print_pkl_training_files()
    #print_meta_pkl_file()
    create_training_pkl_file()
    create_testing_pkl_file()
